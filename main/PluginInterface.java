package main;

public interface PluginInterface {
    public String getName();
    public String getDescription();
    public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack);
}
