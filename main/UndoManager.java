package main;

import java.util.ArrayDeque;
import java.util.LinkedList;

import main.observers.RedoObserver;
import main.observers.UndoObserver;

public class UndoManager { // Singleton
    private ArrayDeque<EditAction> undoStack;
    private ArrayDeque<EditAction> redoStack;
    private static UndoManager instance;
    private LinkedList<UndoObserver> undoObservers;
    private LinkedList<RedoObserver> redoObservers;
    private UndoManager() {
        undoStack = new ArrayDeque<EditAction>();
        redoStack = new ArrayDeque<EditAction>();
        undoObservers = new LinkedList<UndoObserver>();
        redoObservers = new LinkedList<RedoObserver>();
    }

    public static UndoManager getInstance() {
        if(instance == null) {
            instance = new UndoManager();
            return instance;
        }else {
            return instance;
        }
    }

    public void undo() {
        if(!undoStack.isEmpty()) {
            EditAction ea = undoStack.removeFirst();
            ea.execute_undo();
            redoStack.addFirst(ea);
            notifyRedoObservers();
            notifyUndoObservers();
        }
    }

    public void redo() {
        if(!redoStack.isEmpty()) {
            EditAction ea = redoStack.removeFirst();
            ea.execute_do();
            undoStack.addFirst(ea);
            notifyRedoObservers();
            notifyUndoObservers();
        }
    }

    public void push(EditAction ea) {
        redoStack.clear();
        undoStack.addFirst(ea);
        notifyRedoObservers();
        notifyUndoObservers();
    }

    public void addUndoObserver(UndoObserver uo) {
        this.undoObservers.add(uo);
    }

    public void removeUndoObserver(UndoObserver uo) {
        this.undoObservers.remove(uo);
    }

    public void notifyUndoObservers() {
        for(UndoObserver uo : undoObservers) {
            uo.updateUndo(undoStack.size());
        }
    }
    
    public void addRedoObserver(RedoObserver ro) {
        this.redoObservers.add(ro);
    }

    public void removeRedoObserver(RedoObserver ro) {
        this.redoObservers.remove(ro);
    }

    public void notifyRedoObservers() {
        for(RedoObserver ro : redoObservers) {
            ro.updateRedo(redoStack.size());
        }
    }




}
