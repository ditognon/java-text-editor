package main.buttons;

import javax.sound.sampled.Clip;
import javax.swing.JButton;

import main.observers.ClipboardObserver;

public class PasteButton extends JButton implements ClipboardObserver{
    public PasteButton(String text) {
        super(text);
    }
    @Override
    public void updateClipboard(int clipboardSize) {
        if(clipboardSize == 0) {
            this.setEnabled(false);
        }else {
            this.setEnabled(true);
        }
    }
    
}
