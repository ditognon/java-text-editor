package main.menuitems;

import javax.swing.JMenuItem;

import main.observers.UndoObserver;

public class UndoMenuItem extends JMenuItem implements UndoObserver{

    public UndoMenuItem(String text) {
        super(text);
    }

    @Override
    public void updateUndo(int undoStackSize) {
        if(undoStackSize > 0) {
            this.setEnabled(true);
        }else {
            this.setEnabled(false);
        }
    }
    
}
