package main.actions;

import main.EditAction;

public class RealInsertEditAction implements EditAction{
    private EditAction dra;
    private EditAction ira;

    public RealInsertEditAction() {
        dra = null;
        ira = null;
    }

    public void setDeleteRangeEditAction(EditAction dra) {
        this.dra = dra;
    }

    public void setInsertEditAction(EditAction ira) {
        this.ira = ira;
    }

    @Override
    public void execute_do() {
        if(dra != null) {
            dra.execute_do();
        }
        ira.execute_do();
        
    }

    @Override
    public void execute_undo() {
        ira.execute_undo();
        if(dra != null) {
            dra.execute_undo();
        }
        
    }
}
