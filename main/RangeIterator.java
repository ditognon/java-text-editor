package main;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RangeIterator implements Iterator {
    private Iterator it;
    private int index1;
    private int index2;
    private int counter;

    public RangeIterator(Iterator it, int index1, int index2) {
        this.it = it;
        this.index1 = index1;
        this.index2 = index2;
        this.counter = index1;

        // Premotavanje index1 prvih elemenata
        int help = index1;
        while(it.hasNext() && help > 0) {
            it.next();
            help--;
        }
    }

    @Override
    public boolean hasNext() {
        if(this.it.hasNext() && this.counter < this.index2) {
            return true;
        }else {
            return false;
        }
    }

    @Override
    public Object next() {
        if(this.it.hasNext() && this.counter++ < this.index2) {
            return this.it.next();
        }else {
            throw new NoSuchElementException();
        }
    }
    
}
