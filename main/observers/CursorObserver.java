package main.observers;

import main.Location;

public interface CursorObserver {
    public void updateCursorLocation(Location cursorLocation);
}
