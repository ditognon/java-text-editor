package main;

public class LocationRange {
    private Location start;
    private Location end;

    public LocationRange(Location start, Location end) {
        this.start = start;
        this.end = end;
    }

    public LocationRange(LocationRange copy) {
        if(copy != null) {
            this.start = new Location(copy.getStartX(), copy.getStartY());
            this.end = new Location(copy.getEndX(), copy.getEndY());
        }else {
            this.start = null;
            this.end = null;
        }
    }

    public int getStartX() {
        int startx = start.getX();
        int starty = start.getY();
        int endx = end.getX();
        int endy = end.getY();
        if(starty == endy && startx > endx) {
            return endx;
        }else if(starty > endy) {
            return endx;
        }
        return startx;
    }

    public int getStartY() {
        int startx = start.getX();
        int starty = start.getY();
        int endx = end.getX();
        int endy = end.getY();
        if(starty > endy) {
            return endy;
        }
        return starty;
    }

    public int getEndX() {
        int startx = start.getX();
        int starty = start.getY();
        int endx = end.getX();
        int endy = end.getY();
        if(starty == endy && startx > endx) {
            return startx;
        }else if(starty > endy) {
            return startx;
        }
        return endx;
    }

    public int getEndY() {
        int startx = start.getX();
        int starty = start.getY();
        int endx = end.getX();
        int endy = end.getY();
        if(starty > endy) {
            return starty;
        }
        return endy;
    }

    public void setStart(int x, int y) {
        this.start.setX(x);
        this.start.setY(y);
    }

    public void setEnd(int x, int y) {
        this.end.setX(x);
        this.end.setY(y);
    }
}
