package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class EditorListener implements KeyListener{
    private TextEditor textEditor;
    private int shift;
    private boolean ctrl;
    public EditorListener(TextEditor textEditor) {
        this.textEditor = textEditor;
        shift = 32;
        ctrl = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // NOP
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("key pressed " + e.getKeyCode());
        TextEditorModel textEditorModel = textEditor.getModel();
        ClipboardStack clipboardStack = textEditor.getClipboardStack();
        UndoManager undoManager = textEditor.getUndoManager();
        int code = e.getKeyCode();
        switch(code) {
            case KeyEvent.VK_UP:
                textEditorModel.moveCursorUp(shift);
                break;
            case KeyEvent.VK_DOWN:
                textEditorModel.moveCursorDown(shift);
                break;
            case KeyEvent.VK_LEFT:
                textEditorModel.moveCursorLeft(shift);
                break;
            case KeyEvent.VK_RIGHT:
                textEditorModel.moveCursorRight(shift);
                break;
            case KeyEvent.VK_DELETE:
                undoManager.push(textEditorModel.delete());
                break;
            case KeyEvent.VK_BACK_SPACE:
                undoManager.push(textEditorModel.backspace());
                break;
            case KeyEvent.VK_SPACE:
                undoManager.push(textEditorModel.realInsert(' '));
                break;
            case KeyEvent.VK_ENTER:
                undoManager.push(textEditorModel.realInsert("\n"));
                break;
            case KeyEvent.VK_SHIFT:
                shift = 0;
                break;
            case KeyEvent.VK_CONTROL:
                ctrl = true;
                break;
            default:
                if(ctrl) {
                    switch(code) {
                        case KeyEvent.VK_C:
                            if(textEditorModel.getSelectionRange() != null) {
                                clipboardStack.pushToClipboard(textEditorModel.getSelected());
                            }
                            break;
                        case KeyEvent.VK_V:
                            if(shift != 0) {
                                undoManager.push(textEditorModel.realInsert(clipboardStack.peekFromClipboard()));
                            }else {
                                undoManager.push(textEditorModel.realInsert(clipboardStack.popFromClipboard()));
                            }
                            break;
                        case KeyEvent.VK_X:
                            if(textEditorModel.getSelectionRange() != null) {
                                clipboardStack.pushToClipboard(textEditorModel.getSelected());
                                undoManager.push(textEditorModel.deleteRange(textEditorModel.getSelectionRange()));
                                textEditorModel.setSelectionRange(null);
                            }
                            break;
                        case KeyEvent.VK_Z:
                            undoManager.undo();
                            break;
                        case KeyEvent.VK_Y:
                            undoManager.redo();
                            break;
                    }
                }
                else if(code >= 65 && code <= 90) {
                    undoManager.push(textEditorModel.realInsert((char)(code + shift)));
                }else {
                    undoManager.push(textEditorModel.realInsert((char)(code - 16 + (shift/2))));
                // }else {
                //     textEditorModel.insert((char)(code + 16 - (shift/2)));
                // } code >= 48 && code <= 57
                }
        }
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_SHIFT:
                shift = 32;
                break;
            case KeyEvent.VK_CONTROL:
                ctrl = false;
                break;
        }
        
    }
    
}
