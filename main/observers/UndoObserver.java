package main.observers;

public interface UndoObserver {
    public void updateUndo(int undoStackSize);
}
