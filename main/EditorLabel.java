package main;

import javax.swing.JLabel;

import main.observers.CursorObserver;
import main.observers.TextObserver;

public class EditorLabel extends JLabel implements CursorObserver, TextObserver{
    private Location location;
    private int nOfLines;
    public EditorLabel(String text) {
        super(text);
    }

    public void printLabel() {
        this.setText("Cursor location: " + this.location.getX() + ", " + this.location.getY() + "  Lines: " + this.nOfLines);
    }

    @Override
    public void updateCursorLocation(Location cursorLocation) {
       this.location = new Location(cursorLocation);
       printLabel();
    }

    @Override
    public void updateText(int nOfLines) {
        this.nOfLines = nOfLines;
        printLabel();
    }
}
