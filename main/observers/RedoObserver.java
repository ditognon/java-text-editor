package main.observers;

public interface RedoObserver {
    public void updateRedo(int redoStackSize);
}
