package main.actions;

import main.EditAction;
import main.Location;
import main.LocationRange;
import main.TextEditorModel;

public class InsertEditAction implements EditAction {
    private TextEditorModel model;
    private Location beforeCursorLocation;
    private Location afterCursorLocation;
    private String insertedText;
    public void setTextEditorModel(TextEditorModel model) {
        this.model = model;
    }

    public void setBeforeCursorLocation(Location before) {
        this.beforeCursorLocation = new Location(before);
    }

    public void setAfterCursorLocation(Location after) {
        this.afterCursorLocation = new Location(after);
    }

    public void setInsertedText(String inserted) {
        this.insertedText = inserted;
    }

    @Override
    public void execute_do() {
        model.setCursorLocation(beforeCursorLocation.getX(), beforeCursorLocation.getY());
        model.insert(insertedText);
    }

    @Override
    public void execute_undo() {
        model.deleteRange(new LocationRange(beforeCursorLocation, afterCursorLocation));
    }

}
