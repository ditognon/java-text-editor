package main.buttons;

import javax.swing.JButton;

import main.LocationRange;
import main.observers.SelectionObserver;

public class CutButton extends JButton implements SelectionObserver{
    public CutButton(String text) {
        super(text);
    }
    @Override
    public void updateSelection(LocationRange range) {
        if(range == null) {
            this.setEnabled(false);
        }else {
            this.setEnabled(true);
        }
    }
}
