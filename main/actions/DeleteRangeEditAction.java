package main.actions;

import main.EditAction;
import main.Location;
import main.LocationRange;
import main.TextEditorModel;

public class DeleteRangeEditAction implements EditAction{
    private TextEditorModel model;
    private Location beforeCursorLocation;
    private Location afterCursorLocation;
    private String deletedText;
    private LocationRange selectionRange;

    public void setTextEditorModel(TextEditorModel model) {
        this.model = model;
    }

    public void setBeforeCursorLocation(Location before) {
        this.beforeCursorLocation = new Location(before);
    }

    public void setAfterCursorLocation(Location after) {
        this.afterCursorLocation = new Location(after);
    }

    public void setDeletedText(String deleted) {
        this.deletedText = deleted;
    }

    public void setSelectionRange(LocationRange r) {
        this.selectionRange = new LocationRange(r);
    }

    @Override
    public void execute_do() {
        model.deleteRange(selectionRange);
        model.setSelectionRange(null);
        
    }

    @Override
    public void execute_undo() {
        model.setCursorLocation(selectionRange.getStartX(), selectionRange.getStartY());
        model.insert(deletedText);

        model.setSelectionRange(selectionRange);
        
    }
    

}
