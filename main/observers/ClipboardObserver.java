package main.observers;

public interface ClipboardObserver {
    public void updateClipboard(int clipboardSize);
}
