package main.plugins;
import java.util.Iterator;
import java.util.LinkedList;

import main.*;

public class VelikoSlovoPlugin implements PluginInterface{

    @Override
    public String getName() {
        return "Veliko Slovo Plugin";
    }

    @Override
    public String getDescription() {
        return "Pretvara svako pocetno slovo rijeci u veliko slovo!";
    }

    @Override
    public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
        Iterator it = model.allLines();

        LinkedList<String> oldLines = new LinkedList<String>();
        LinkedList<String> newLines = new LinkedList<String>();
        while(it.hasNext()) {
            oldLines.add(it.next().toString());
        }

        String tmpNewLine;
        for(String line : oldLines) {
            tmpNewLine = "";
            for(String word : line.split(" ")) {
                tmpNewLine += word.substring(0, 1).toUpperCase() + word.substring(1) + " ";
            }
            newLines.add(tmpNewLine);
        }

        for(String line : newLines) {
            System.out.println(line);
        }
        
        model.setLines(newLines);
    }
    
}
