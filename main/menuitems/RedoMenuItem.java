package main.menuitems;

import javax.swing.JMenuItem;

import main.observers.RedoObserver;

public class RedoMenuItem extends JMenuItem implements RedoObserver{
    public RedoMenuItem(String text) {
        super(text);
    }
    @Override
    public void updateRedo(int redoStackSize) {
        if(redoStackSize > 0) {
            this.setEnabled(true);
        }else {
            this.setEnabled(false);
        }
    }
}
