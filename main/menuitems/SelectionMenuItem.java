package main.menuitems;

import javax.swing.JMenuItem;

import main.LocationRange;
import main.observers.SelectionObserver;

public class SelectionMenuItem extends JMenuItem implements SelectionObserver{
    public SelectionMenuItem(String text) {
        super(text);
    }

    @Override
    public void updateSelection(LocationRange range) {
        if(range == null) {
            this.setEnabled(false);
        }else {
            this.setEnabled(true);
        }
    }
    
}
