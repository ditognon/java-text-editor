package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;

import main.buttons.CutButton;
import main.buttons.PasteButton;
import main.buttons.RedoButton;
import main.buttons.UndoButton;
import main.menuitems.*;

public class Main {
    public final static int frameX = 600;
    public final static int frameY = 400;
    public final static int fontSize = 20;

    public static LinkedList<String> getPluginNames(String path) {
        File folder = new File(".\\main\\plugins");
        File[] listOfFiles = folder.listFiles();

        LinkedList<String> names = new LinkedList<String>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                String fullName = listOfFiles[i].getName();
                System.out.println(fullName);
                if(fullName.endsWith(".java")) {
                    String res = fullName.substring(0, fullName.lastIndexOf(".java"));
                    names.add(res);
                    System.out.println(res);
                }
            }
        }

        return names;
    }
    public static void main(String args[]) {
        TextEditor editor = new TextEditor();
        TextEditorModel tem = new TextEditorModel("Prva recenica je malo duza.\n" +
        "Katastrofa moze biti velika.\n" +
        "Druga recenica.\n" +
        "Kraj je isto malo duzi hehe!");
        ClipboardStack clipboardStack = new ClipboardStack();
        UndoManager undoManager = UndoManager.getInstance();
        editor.setTextEditorModel(tem);
        editor.setClipboardStack(clipboardStack);
        editor.setUndoManager(undoManager);
        
        EditorListener el = new EditorListener(editor);
        JFrame mainFrame = new JFrame();
        mainFrame.setLayout(new BorderLayout());

        tem.addCursorObserver(editor);
        tem.addTextObserver(editor);

        JMenuBar menubar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu moveMenu = new JMenu("Move");

        JMenuItem open = new JMenuItem("Open");
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //mainFrame.dispose();
                System.out.println("Open");
            }
        });
        JMenuItem save = new JMenuItem("Save");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Save");
            }
        });
        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.dispose();
            }  
        });

        fileMenu.add(open);
        fileMenu.add(save);
        fileMenu.add(exit);

        UndoMenuItem undo = new UndoMenuItem("Undo");
        undo.setEnabled(false);
        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.getUndoManager().undo();
            }
        });
        undoManager.addUndoObserver(undo);

        RedoMenuItem redo = new RedoMenuItem("Redo");
        redo.setEnabled(false);
        redo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.getUndoManager().redo();
            }
        });
        undoManager.addRedoObserver(redo);
        
        SelectionMenuItem cut = new SelectionMenuItem("Cut");
        cut.setEnabled(false);
        cut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clipboardStack.pushToClipboard(tem.getSelected());
                undoManager.push(tem.deleteRange(tem.getSelectionRange()));
                tem.setSelectionRange(null);
                
            }
        });
        editor.getModel().addSelectionObserver(cut);

        SelectionMenuItem copy = new SelectionMenuItem("Copy");
        copy.setEnabled(false);
        copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clipboardStack.pushToClipboard(tem.getSelected());
            }
        });
        editor.getModel().addSelectionObserver(copy);

        ClipboardMenuItem paste = new ClipboardMenuItem("Paste");
        paste.setEnabled(false);
        paste.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undoManager.push(tem.realInsert(clipboardStack.peekFromClipboard()));
            }
        });
        editor.getClipboardStack().addClipboardObserver(paste);

        ClipboardMenuItem pasteAndTake = new ClipboardMenuItem("Paste and take");
        //Ne bi trebao biti ClipboardMenuItem jer treba depth clipboada biti barem 2
        pasteAndTake.setEnabled(false);
        pasteAndTake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undoManager.push(tem.realInsert(clipboardStack.popFromClipboard()));
            }
        });
        editor.getClipboardStack().addClipboardObserver(pasteAndTake);;
        
        SelectionMenuItem deleteSelection = new SelectionMenuItem("Delete selection");
        deleteSelection.setEnabled(false);
        deleteSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tem.deleteRange(tem.getSelectionRange());
                tem.setSelectionRange(null);
            } 
        });
        editor.getModel().addSelectionObserver(deleteSelection);

        JMenuItem clearDocument = new JMenuItem("Clear document");
        clearDocument.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String line = null;
                Iterator it = tem.allLines();
                int i = 0;
                while(it.hasNext())  {
                    i++;
                    line = it.next().toString();
                }

                tem.deleteRange(new LocationRange(new Location(0, 0), new Location(line.length(), i)));
            }
        });

        editMenu.add(undo);
        editMenu.add(redo);
        editMenu.add(cut);
        editMenu.add(copy);
        editMenu.add(paste);
        editMenu.add(pasteAndTake);
        editMenu.add(deleteSelection);
        editMenu.add(clearDocument);

        JMenuItem cursorToDocStart = new JMenuItem("Cursor to document start");
        cursorToDocStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tem.setCursorLocation(0, 0);
                editor.repaint();
            }
        });

        JMenuItem cursorToDocEnd = new JMenuItem("Cursor to document end");
        cursorToDocEnd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String line = null;
                Iterator it = tem.allLines();
                int i = 0;
                while(it.hasNext())  {
                    i++;
                    line = it.next().toString();
                }

                tem.setCursorLocation(line.length(), i - 1);
                editor.repaint();
            }
        });

        JToolBar toolbar = new JToolBar();
        UndoButton undoButton = new UndoButton("Undo");
        undoButton.setEnabled(false);
        undoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.getUndoManager().undo();
            }
        });
        undoManager.addUndoObserver(undoButton);

        RedoButton redoButton = new RedoButton("Redo");
        redoButton.setEnabled(false);
        redoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.getUndoManager().redo();
            } 
        });
        undoManager.addRedoObserver(redoButton);

        CutButton cutButton = new CutButton("Cut");
        cutButton.setEnabled(false);
        cutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clipboardStack.pushToClipboard(tem.getSelected());
                undoManager.push(tem.deleteRange(tem.getSelectionRange()));
                tem.setSelectionRange(null);
            } 
        });
        tem.addSelectionObserver(cutButton);

        CutButton copyButton = new CutButton("Copy");
        copyButton.setEnabled(false);
        copyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clipboardStack.pushToClipboard(tem.getSelected());
            } 
        });
        tem.addSelectionObserver(copyButton);

        PasteButton pasteButton = new PasteButton("Paste");
        pasteButton.setEnabled(false);
        pasteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undoManager.push(tem.realInsert(clipboardStack.peekFromClipboard()));
            } 
        });
        clipboardStack.addClipboardObserver(pasteButton);

        JMenu plugins = new JMenu("Plugins");

        for(String name : getPluginNames("./plugins")) {

            Class<PluginInterface> clazz = null;
            try {
                clazz = (Class<PluginInterface>) Class.forName("main.plugins." + name);
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }

            PluginInterface tempPlugin;
            try {
                tempPlugin = (PluginInterface)clazz.newInstance();
                JMenuItem temp = new JMenuItem(tempPlugin.getName());
                plugins.add(temp);
                temp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    tempPlugin.execute(editor.getModel(), editor.getUndoManager(), editor.getClipboardStack());
                    editor.repaint();
                } 
                });
            } catch (InstantiationException | IllegalAccessException e1) {
                e1.printStackTrace();
            }
        }

        menubar.add(plugins);

        EditorLabel statusBar = new EditorLabel("Cursor location: "+  "  Lines: ");
        editor.getModel().addCursorObserver(statusBar);
        editor.getModel().addTextObserver(statusBar);
        statusBar.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        mainFrame.add(statusBar, BorderLayout.SOUTH);

        toolbar.add(undoButton);
        toolbar.add(redoButton);
        toolbar.add(cutButton);
        toolbar.add(copyButton);
        toolbar.add(pasteButton);
        
        moveMenu.add(cursorToDocStart);
        moveMenu.add(cursorToDocEnd);

        menubar.add(fileMenu);
        menubar.add(editMenu);
        menubar.add(moveMenu);

        mainFrame.setJMenuBar(menubar);
        mainFrame.add(toolbar, BorderLayout.NORTH);

        editor.setFocusable(true);
        editor.requestFocus();
        toolbar.setFocusable(false);
        mainFrame.add(editor, BorderLayout.CENTER);
        editor.addKeyListener(el);
        mainFrame.setVisible(true);
        mainFrame.setSize(frameX, frameY);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
