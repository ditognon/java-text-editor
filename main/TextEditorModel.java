package main;

import java.util.Iterator;
import java.util.LinkedList;

import main.actions.DeleteAfterEditAction;
import main.actions.DeleteBeforeEditAction;
import main.actions.DeleteRangeEditAction;
import main.actions.InsertEditAction;
import main.actions.RealInsertEditAction;
import main.observers.CursorObserver;
import main.observers.SelectionObserver;
import main.observers.TextObserver;

public class TextEditorModel {
    private LinkedList<String> lines;
    private Location cursorLocation;
    private LocationRange selectionRange;

    private LinkedList<CursorObserver> cursorObservers;
    private LinkedList<TextObserver> textObservers;
    private LinkedList<SelectionObserver> selectionObservers;

    public TextEditorModel(String initial) {
        lines = new LinkedList<String>();
        cursorLocation = new Location(0,0);
        cursorObservers = new LinkedList<CursorObserver>();
        textObservers = new LinkedList<TextObserver>();
        selectionObservers = new LinkedList<SelectionObserver>();
        selectionRange = null;

        for(String s : initial.split("\n")) {
            lines.add(s);
        }
    }

    public Location getCursorLocation() {
        return this.cursorLocation;
    }

    public void setCursorLocation(int x, int y) {
        this.cursorLocation.setX(x);
        this.cursorLocation.setY(y);
    }

    public Iterator allLines() {
        return this.lines.iterator();
    }

    public Iterator linesRange(int index1, int index2) {
        return new RangeIterator(this.lines.iterator(), index1, index2);
    }

    public void addCursorObserver(CursorObserver observer) {
        this.cursorObservers.add(observer);
    }

    public void removeCursorObserver(CursorObserver observer) {
        this.cursorObservers.remove(observer);
    }

    public void notifyCursorObservers() {
        for(CursorObserver co : this.cursorObservers) {
            co.updateCursorLocation(this.cursorLocation);
        }
    }

    public void addSelectionObserver(SelectionObserver so) {
        this.selectionObservers.add(so);
    }

    public void deleteSelectionObserver(SelectionObserver so) {
        this.selectionObservers.remove(so);
    }

    public void notifySelectionObservers() {
        for(SelectionObserver so : this.selectionObservers) {
            so.updateSelection(this.getSelectionRange());
        }
    }

    public void addTextObserver(TextObserver observer) {
        this.textObservers.add(observer);
    }

    public void removeTextObserver(TextObserver observer) {
        this.textObservers.remove(observer);
    }

    public void notifyTextObservers() {
        for(TextObserver co : this.textObservers) {
            co.updateText(lines.size());
        }
    }

    public void updateSelection(int sx, int sy, int ex, int ey) {
        if(selectionRange == null) {
            setSelectionRange(new LocationRange(new Location(sx, sy), new Location(ex, ey)));
        }else {
            selectionRange.setEnd(ex, ey);
        }
    }

    public void moveCursorUp(int shift) {
        int y = cursorLocation.getY();
        int x = cursorLocation.getX();
        if(y >= 1) {
            if(!(cursorLocation.getX() < lines.get(y - 1).length())) {
                cursorLocation.setX(lines.get(y - 1).length());
            }
            cursorLocation.setY(y - 1);
            notifyCursorObservers();

            if(shift == 0) {
                updateSelection(x, y, cursorLocation.getX(), cursorLocation.getY());
            }else {
                selectionRange = null;
            }
        }
    }

    public void moveCursorDown(int shift) {
        int y = cursorLocation.getY();
        int x = cursorLocation.getX();
        if(y < lines.size() - 1) {
            if(!(cursorLocation.getX() < lines.get(y + 1).length())) {
                cursorLocation.setX(lines.get(y + 1).length());
            }
            cursorLocation.setY(y + 1);
            notifyCursorObservers();

            if(shift == 0) {
                updateSelection(x, y, cursorLocation.getX(), cursorLocation.getY());
            }else {
                selectionRange = null;
            }
        }
    }

    public void moveCursorLeft(int shift) {
        int y = cursorLocation.getY();
        int x = cursorLocation.getX();
        if(x >= 1) {
            cursorLocation.setX(x - 1);
            notifyCursorObservers();

            if(shift == 0) {
                updateSelection(x, y, cursorLocation.getX(), cursorLocation.getY());
            }else {
                selectionRange = null;
            }
        }
    }

    public void moveCursorRight(int shift) {
        int y = cursorLocation.getY();
        int x = cursorLocation.getX();
        if(x < lines.get(cursorLocation.getY()).length()) {
            cursorLocation.setX(x + 1);
            notifyCursorObservers();

            if(shift == 0) {
                updateSelection(x, y, cursorLocation.getX(), cursorLocation.getY());
            }else {
                selectionRange = null;
            }
        }
    }

    public EditAction backspace() {
        if(selectionRange != null) {
            EditAction a = deleteRange(selectionRange);
            setSelectionRange(null);
            return a;
        }else {
            return deleteBefore();
        }
    }

    public EditAction delete() {
        if(selectionRange != null) {
           EditAction a = deleteRange(selectionRange);
           setSelectionRange(null);
           return a;
        }else {
            return deleteAfter();
        }
    }

    //Backspace
    public EditAction deleteBefore() {
        DeleteBeforeEditAction ea = new DeleteBeforeEditAction();
        int x = cursorLocation.getX();
        int y = cursorLocation.getY();
        ea.setTextEditorModel(this);
        ea.setBeforeCursorLocation(cursorLocation);
        if(x >= 1) {
            String firstPart = lines.get(y).substring(0, x - 1);
            String secondPart = lines.get(y).substring(x);
            ea.setDeletedText(Character.toString(lines.get(y).charAt(x-1)));
            String result = firstPart + secondPart;
            lines.remove(y);
            lines.add(y, result);
            moveCursorLeft(1);
            notifyTextObservers();
        }else {
            if(y >= 1) {
                String activeLine = lines.get(y);
                String lineAbove = lines.get(y - 1);
                cursorLocation.setX(lines.get(y - 1).length());
                cursorLocation.setY(y - 1);
                lines.remove(y - 1);
                lines.add(y - 1, lineAbove + activeLine);
                lines.remove(y);
                notifyCursorObservers();
                notifyTextObservers();

                ea.setDeletedText("\n");
            }
        }

        ea.setAfterCursorLocation(cursorLocation);

        return ea;
    }

    // Delete - implementirati odlazak u novi red
    public EditAction deleteAfter() {
        DeleteAfterEditAction da = new DeleteAfterEditAction();
        int x = cursorLocation.getX();
        int y = cursorLocation.getY();

        da.setTextEditorModel(this);
        da.setBeforeCursorLocation(cursorLocation);

        if(x < lines.get(y).length()) {
            String firstPart = lines.get(y).substring(0, x);
            String secondPart = lines.get(y).substring(x + 1);
            da.setDeletedText(Character.toString(lines.get(y).charAt(x)));
            String result = firstPart + secondPart;
            lines.remove(y);
            lines.add(y, result);
            notifyTextObservers();
        }

        da.setAfterCursorLocation(cursorLocation);

        return da;
    }

    public EditAction deleteRange(LocationRange r) {
        DeleteRangeEditAction dra = new DeleteRangeEditAction();
        int starty = r.getStartY();
        int startx = r.getStartX();
        int endy = r.getEndY();
        int endx = r.getEndX();

        dra.setTextEditorModel(this);
        dra.setBeforeCursorLocation(cursorLocation);
        dra.setDeletedText(getSelected());
        dra.setSelectionRange(selectionRange);

        if(starty >= 0 && starty < lines.size() && endy >= 0 && endy < lines.size()) {
            if(startx < lines.get(starty).length() && endx < lines.get(endy).length())  {
                if(starty == endy) { // Cijela selekcija u istom redku
                    String beforeDelete = lines.get(starty).substring(0, startx);
                    String afterDelete = lines.get(starty).substring(endx);
                    lines.remove(starty);
                    lines.add(starty, beforeDelete + afterDelete);
                    cursorLocation.setX(startx);
                }else { // Selekcija se proteze kroz vise redaka
                    //Obraditi prvi redak
                    String firstSave = lines.get(starty).substring(0, startx);
                    int cursorY = starty;
                    LinkedList<Integer> toDelete = new LinkedList<Integer>();
                    // Brisanje svih redaka u selekciji
                    while(starty != endy) {
                        toDelete.add(starty);
                        starty++;
                    }

                    //Obrada zadnjeg retka u kojem je nesto odabrano
                    String lastSave = lines.get(endy).substring(endx);
                    lines.remove(endy);
                    lines.add(starty, firstSave + lastSave);

                    for(int i : toDelete) {
                        lines.remove(i);
                    }

                    cursorLocation.setX(startx);
                    cursorLocation.setY(cursorY);
                }
            }
        }

        dra.setAfterCursorLocation(cursorLocation);

        notifyTextObservers();
        notifyCursorObservers();

        return dra;
    }

    public EditAction realInsert(char c) {
        RealInsertEditAction rea = new RealInsertEditAction();
        if(selectionRange != null) {
            rea.setDeleteRangeEditAction(deleteRange(selectionRange));
            setSelectionRange(null);
        }

        rea.setInsertEditAction(insert(c));

        return rea;
    }

    public EditAction realInsert(String text) {
        RealInsertEditAction rea = new RealInsertEditAction();
        if(selectionRange != null) {
            rea.setDeleteRangeEditAction(deleteRange(selectionRange));
            setSelectionRange(null);
        }

        rea.setInsertEditAction(insert(text));

        return rea;
    }

    public EditAction insert(char c) {
        return insert(Character.toString(c));
    }

    public EditAction insert(String text) {
        InsertEditAction iea = new InsertEditAction();
        int y = cursorLocation.getY();
        int x = cursorLocation.getX();

        iea.setTextEditorModel(this);
        iea.setInsertedText(text);
        iea.setBeforeCursorLocation(cursorLocation);

        String line = lines.get(y);

        String before = line.substring(0, x);
        String after = line.substring(x);

        LinkedList<String> newLines = new LinkedList<String>();

        for(String s : text.split("\n")) {
            newLines.add(s);
        }

        if(text.equals("\n")) {
            newLines.add("");
            newLines.add("");
        }

        if(newLines.size() > 1) {
            LinkedList<String> resLines = new LinkedList<String>();
            resLines.add(before + newLines.get(0));
            for(int i = 1; i < newLines.size(); i++) {
                if(i == newLines.size() - 1) {
                    resLines.add(newLines.get(i) + after);
                }else {
                    resLines.add(newLines.get(i));
                }
            }

            lines.remove(y);
            lines.addAll(y, resLines);

            cursorLocation.setX(newLines.getLast().length());
            cursorLocation.setY(y + resLines.size() - 1);
        }else {
            String result = before + text + after;
            lines.remove(y);
            lines.add(y, result);
            cursorLocation.setX(x + text.length());
        }
        notifyCursorObservers();
        notifyTextObservers();

        iea.setAfterCursorLocation(cursorLocation);
        return iea;
    }

    public LocationRange getSelectionRange() {
        return this.selectionRange;
    }

    public void setSelectionRange(LocationRange r) {
        this.selectionRange = r;
        notifySelectionObservers();
    }

    public String getSelected() {
        String result = null;
        if(selectionRange != null) {
            int starty = selectionRange.getStartY();
            int startx = selectionRange.getStartX();
            int endy = selectionRange.getEndY();
            int endx = selectionRange.getEndX();

            if(starty != endy) {
                result = lines.get(starty).substring(startx) + "\n";
                starty++;
                while(starty != endy) {
                    result += lines.get(starty) + "\n";
                    starty++;
                }
                result += lines.get(endy).substring(0, endx); // +1 potencijalan problem!
            }else {
                result = lines.get(starty).substring(startx, endx); // Ovaj +1 potencijalan problem!
            }
        }

        return result;
    }

    public void setLines(LinkedList<String> newLines) {
        this.lines = newLines;
    }
}
