package main.actions;

import main.EditAction;
import main.Location;
import main.TextEditorModel;

public class DeleteAfterEditAction implements EditAction{
    private TextEditorModel model;
    private Location beforeCursorLocation;
    private Location afterCursorLocation;
    private String deletedText;
    public void setTextEditorModel(TextEditorModel model) {
        this.model = model;
    }

    public void setBeforeCursorLocation(Location before) {
        this.beforeCursorLocation = new Location(before);
    }

    public void setAfterCursorLocation(Location after) {
        this.afterCursorLocation = new Location(after);
    }

    public void setDeletedText(String deleted) {
        this.deletedText = deleted;
    }

    @Override
    public void execute_do() {
        model.setCursorLocation(beforeCursorLocation.getX(), beforeCursorLocation.getY());
        model.deleteAfter();
    }

    @Override
    public void execute_undo() {
        model.insert(deletedText);
        model.setCursorLocation(afterCursorLocation.getX(), afterCursorLocation.getY());
    }
}
