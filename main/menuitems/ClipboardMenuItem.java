package main.menuitems;
import javax.swing.JMenuItem;

import main.observers.ClipboardObserver;

public class ClipboardMenuItem extends JMenuItem implements ClipboardObserver{
    public ClipboardMenuItem(String text) {
        super(text);
    }
    @Override
    public void updateClipboard(int clipboardSize) {
        if(clipboardSize == 0) {
            this.setEnabled(false);
        }else {
            this.setEnabled(true);
        }
    }

}
