package main;

import java.awt.*;
import java.util.Iterator;

import javax.swing.JPanel;

import main.observers.CursorObserver;
import main.observers.TextObserver;

public class TextEditor extends JPanel implements CursorObserver, TextObserver {
    private TextEditorModel model;
    private UndoManager undoManager;
    private ClipboardStack clipboardStack;
    private final static int selectOffsetConst = 5;

    public TextEditor() {
        super();
    }

    public void setUndoManager(UndoManager undoManager) {
        this.undoManager = undoManager;
    }

    public void setTextEditorModel(TextEditorModel model) {
        this.model = model;
    }

    public void setClipboardStack(ClipboardStack clipboardStack) {
        this.clipboardStack = clipboardStack;
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setFont(new Font("Monospaced", Font.PLAIN, Main.fontSize)); 
        g.setColor(Color.BLACK);

        int linePos = Main.fontSize;
        Iterator it = this.model.allLines();
        while(it.hasNext()) {
            g.drawString(it.next().toString(), 5, linePos);
            linePos += Main.fontSize;
        }

        g.setColor(Color.RED); // Privremeno da vidim gdje je kursor!!
        Location cursorLocation = this.model.getCursorLocation();
        g.drawString("|", cursorLocation.getX() * 12, (cursorLocation.getY() * Main.fontSize) + Main.fontSize);

        g.setColor(new Color(50, 150, 150, 100));
        LocationRange selection = this.model.getSelectionRange();
        if(selection != null) {
            int starty = selection.getStartY();
            int startx = selection.getStartX();
            int endy = selection.getEndY();
            int endx = selection.getEndX();
            
            if(starty != endy) {
                Iterator itr = this.model.linesRange(starty, endy);
                String line;
                line = itr.next().toString();
                g.fillRect(startx * 12 + selectOffsetConst, (starty * Main.fontSize),
                (line.length() - startx) * 12, Main.fontSize);
                starty++;
                while(itr.hasNext()) {
                    line = itr.next().toString();
                    g.fillRect(0 + selectOffsetConst, (starty * Main.fontSize), 
                        line.length() * 12, Main.fontSize);
                        starty++;
                }
                g.fillRect(0 + selectOffsetConst, (starty * Main.fontSize), 
                endx * 12, Main.fontSize);
            }else {
                g.fillRect(startx * 12 + selectOffsetConst, (starty * Main.fontSize),
            (endx - startx) * 12, Main.fontSize);
            }
        }
    }

    public TextEditorModel getModel() {
        return this.model;
    }

    public UndoManager getUndoManager() {
        return this.undoManager;
    }

    public ClipboardStack getClipboardStack() {
        return this.clipboardStack;
    }

    @Override
    public void updateCursorLocation(Location cursorLocation) {
        this.repaint();
    }

    @Override
    public void updateText(int nOfLines) {
        this.repaint();
        
    }
}
