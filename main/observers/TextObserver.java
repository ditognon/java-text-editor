package main.observers;

public interface TextObserver {
    public void updateText(int nOfLines);
}
