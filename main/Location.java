package main;

public class Location {
    private int x;
    private int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Location(Location copy) {
        if(copy != null) {
            this.x = copy.x;
            this.y = copy.y;
        }else { // Prevencija errora
            System.out.println("NULL LOCATION!");
            this.x = 0;
            this.y = 0;
        }
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        if(x >= 0) {
            this.x = x;
        }
    }

    public void setY(int y) {
        if(y >= 0) {
            this.y = y;
        }
    }
}
