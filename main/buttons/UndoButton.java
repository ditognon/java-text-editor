package main.buttons;

import javax.swing.JButton;

import main.observers.UndoObserver;

public class UndoButton extends JButton implements UndoObserver{
    public UndoButton(String text) {
        super(text);
    }

    @Override
    public void updateUndo(int undoStackSize) {
        if(undoStackSize > 0) {
            this.setEnabled(true);
        }else {
            this.setEnabled(false);
        }
    }
}
