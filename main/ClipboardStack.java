package main;

import java.util.ArrayDeque;
import java.util.LinkedList;

import main.observers.ClipboardObserver;

public class ClipboardStack {
    private ArrayDeque<String> texts;
    private LinkedList<ClipboardObserver> observers;

    public ClipboardStack() {
        texts = new ArrayDeque<String>();
        observers = new LinkedList<>();
    }

    public void pushToClipboard(String text) {
        texts.addFirst(text);
        notifyClipboardObservers();
    }

    public String popFromClipboard() {
        notifyClipboardObservers();
        return texts.removeFirst();
    }

    public String peekFromClipboard() {
        return texts.peekFirst();
    }

    public boolean containsElements() {
        if(texts.size() == 0) {
            return false;
        }
        return true;
    }

    public void addClipboardObserver(ClipboardObserver co) {
        this.observers.add(co);
    }

    public void removeClipboardObserver(ClipboardObserver co) {
        this.observers.remove(co);
    }

    public void notifyClipboardObservers() {
        for(ClipboardObserver co : observers) {
            co.updateClipboard(texts.size());
        }
    }
}
