package main.buttons;

import javax.swing.JButton;

import main.observers.RedoObserver;

public class RedoButton extends JButton implements RedoObserver{
    public RedoButton(String text) {
        super(text);
    }
    @Override
    public void updateRedo(int redoStackSize) {
        if(redoStackSize > 0) {
            this.setEnabled(true);
        }else {
            this.setEnabled(false);
        }
    }
    
}