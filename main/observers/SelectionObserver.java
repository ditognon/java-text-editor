package main.observers;

import main.LocationRange;

public interface SelectionObserver {
    public void updateSelection(LocationRange range);
}
