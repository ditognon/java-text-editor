package main.plugins;

import java.util.Iterator;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import main.ClipboardStack;
import main.PluginInterface;
import main.TextEditorModel;
import main.UndoManager;

public class StatistikaPlugin implements PluginInterface{

    @Override
    public String getName() {
        return "Statistika Plugin";
    }

    @Override
    public String getDescription() {
        return "Ispisuje statistiku u zasebni dialog";
    }

    @Override
    public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
        int rows = 0;
        int words = 0;
        int letters = 0;

        Iterator it = model.allLines();
        String currentLine;
        while(it.hasNext()) {
            currentLine = it.next().toString();
            rows++;

            String[] rijeci = currentLine.split(" ");
            words += rijeci.length;

            for(char c : currentLine.toCharArray()) {
                if(Character.isAlphabetic(c)) {
                    letters++;
                }
            }
        }

        JDialog dialog = new JDialog();
        JLabel text = new JLabel();

        text.setText("Redaka: " + rows + "  Rijeci: " + words + "  Slova: " + letters);

        dialog.add(text);
        dialog.setVisible(true);
        //dialog.setSize(frameX, frameY);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
    
}
